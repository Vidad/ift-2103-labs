﻿using UnityEngine;
using System.Collections;

public class ObjectBehaviour : MonoBehaviour
{

    public float jumpSpeed = 5.0F;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Jump"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, jumpSpeed, 0);
        }       
    }


}


