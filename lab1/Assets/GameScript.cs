﻿using UnityEngine;
using System.Collections;

public class GameScript : MonoBehaviour {

    public GameObject duckPrefab;
    float deltaTime = 0.0f;

    // Use this for initialization
    void Start () {
        float xOffset = 10;
        float zOffset = 10;
        for (int index = 0; index < 3000; index++)
        {

            if(index % 10 == 0 && index != 0)
            {
                xOffset += 1.5f;
                zOffset = 10;
            }
           

            Instantiate(duckPrefab, new Vector3(xOffset, 1, zOffset), new Quaternion());
            zOffset += 1.5f;
        }
    }
	
	// Update is called once per frame
	void Update () {
        updateFPS();
    }

    private void updateFPS()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        Debug.Log(text);

        UnityEngine.UI.Text fpsGuiValue = GameObject.Find("FPS_value").GetComponent<UnityEngine.UI.Text>();
        fpsGuiValue.text = text;
    }
}
