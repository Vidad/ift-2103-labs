﻿using UnityEngine;
using System.Collections;

public class avatarController : MonoBehaviour {

    private Rigidbody2D rigidbody;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        float speed = 0.5f;
        float translation = Input.GetAxis("Horizontal") * speed;
        transform.Translate(translation, 0, 0);

        if (Input.GetButtonDown("Jump"))
        {
            rigidbody.AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
        }
    
    }
}
