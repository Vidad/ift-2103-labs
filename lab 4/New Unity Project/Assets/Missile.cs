﻿using System;
using UnityEngine;

internal class Missile:Projectile
{
    public GameObject explosion;

    public Missile()
    {
    }

    override public void shoot(Vector3 shooterPosition)
    {
        transform.position = shooterPosition;
        Rigidbody2D rigidBody = this.GetComponent<Rigidbody2D>();
        rigidBody.gravityScale = 0;
        rigidBody.isKinematic = false;
        int force = 10;
        rigidBody.AddForce(transform.up * force, ForceMode2D.Impulse);
        Debug.Log("Missile position: " + transform.position);
        Debug.Log("Missile fired");
    }

    override public void explose()
    {
        UnityEngine.Object.Instantiate(explosion, transform.position, transform.rotation);
    }
}