﻿using UnityEngine;
using System.Collections;

public class BackgroundScroll : MonoBehaviour {


    private float scrollSpeed = 1f;
    private float tileSizeY = 28.0f;

    private Vector3 startPosition;

    // Use this for initialization
    void Start () {
        startPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        float newPosition = Mathf.Repeat(Time.time * scrollSpeed *-1, tileSizeY);
        transform.position = startPosition + Vector3.up * newPosition;
    }
}