﻿using UnityEngine;
using System.Collections;

public class explosable : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "EnnemyJet")
        {
            Debug.Log("Target is shot");


            Projectile projectile = this.GetComponent<Projectile>();
            projectile.explose();

            Destroy(other.gameObject);
            Renderer renderer = this.GetComponent<Renderer>();
            renderer.enabled = false;
            Destroy(this);
        }
                
    }

}
