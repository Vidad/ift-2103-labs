﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class fighter : MonoBehaviour {

    private List<GameObject> projectilePoolReady;
    private List<GameObject> projectilePoolActive;
    public int projectilePoolSize = 20;

    public GameObject originalBullet;
    public GameObject originalMissile;
    public float shootDelayInSeconds = 0.5f;

    private DateTime lastShootTime;
    private DateTime lastMissileShootTime;

    public AudioSource bulletFiredSound;
    public AudioSource missileFiredSound;

    // Use this for initialization
    void Start () {

        projectilePoolReady = new List<GameObject>();
        projectilePoolActive = new List<GameObject>();

        ProjectileFactory factory = new ProjectileFactory();
        for (int i = 0; i < projectilePoolSize; i++)
        {
            projectilePoolReady.Add(factory.create(Projectile.projectileType.BULLET, originalBullet));
        }

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Vector2 initialPosition = transform.position;
            transform.position = initialPosition - new Vector2(0.2f, 0);
        }
         else if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector2 initialPosition = transform.position;
            transform.position = initialPosition + new Vector2(0.2f, 0);
        }
        else if (Input.GetMouseButton(0))
        {
            shootSignal();
        }
        else if (Input.GetMouseButton(1))
        {
            shootMissileSignal();
        }

    }

    private void shootMissileSignal()
    {
        if (shootMissileDelayIsPassed())
        {
            shootMissile();
        }
    }

    private void shootMissile()
    {
        missileFiredSound.Play();

        lastMissileShootTime = DateTime.Now;
        ProjectileFactory factory = new ProjectileFactory();
        GameObject projectile = factory.create(Projectile.projectileType.MISSILE, originalMissile);
        Missile missile = projectile.GetComponent<Missile>() as Missile;
        Debug.Log("Jet position: " + transform.position);
        missile.shoot(transform.position);
        Debug.Log("shoot");
    }

    private void shootSignal()
    {
        if(projectilePoolReady.Count > 0 && shootBulletDelayIsPassed())
        {
            shootBullet();
        }
    }

    private bool shootBulletDelayIsPassed()
    {
        TimeSpan shootDelay = TimeSpan.FromSeconds(shootDelayInSeconds);

        if(DateTime.Now - lastShootTime < shootDelay)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private bool shootMissileDelayIsPassed()
    {
        TimeSpan shootDelay = TimeSpan.FromSeconds(shootDelayInSeconds);
        
        if (DateTime.Now - lastMissileShootTime < shootDelay)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private void shootBullet()
    {
        bulletFiredSound.Play();

        lastShootTime = DateTime.Now;
        GameObject projectile = projectilePoolReady[0];
        projectilePoolActive.Add(projectile);
        projectilePoolReady.RemoveAt(0);
        Bullet bullet = projectile.GetComponent<Bullet>() as Bullet;
        Debug.Log("Jet position: "+transform.position);
        bullet.shoot(transform.position);
        Debug.Log("shoot");
    }
}
