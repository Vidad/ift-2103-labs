﻿using System;
using UnityEngine;

internal class ProjectileFactory
{

    internal GameObject create(Projectile.projectileType typeOfProjectile, GameObject original)
    {
        if (typeOfProjectile.Equals(Projectile.projectileType.BULLET))
        {
            GameObject newBullet = UnityEngine.Object.Instantiate(original);
            return newBullet;
        }
        else if (typeOfProjectile.Equals(Projectile.projectileType.MISSILE))
        {
            GameObject newMissile = UnityEngine.Object.Instantiate(original);
            return newMissile;
        }
        else
        {
            throw new Exception();
        }
    }
}