﻿using UnityEngine;

internal class Bullet : Projectile
{
    public GameObject explosion;

    public Bullet()
    {
    }

    override public void shoot(Vector3 shooterPosition) {
        transform.position = shooterPosition;
        Rigidbody2D rigidBody = this.GetComponent<Rigidbody2D>();
        rigidBody.gravityScale = 0;
        rigidBody.isKinematic = false;
        int force = 10;
        rigidBody.AddForce(transform.up * force, ForceMode2D.Impulse);
        Debug.Log("Bullet position: " + transform.position);
        Debug.Log("Bullet fired");
    }

    override public void explose()
    {
        Quaternion quaternion = new Quaternion();
        quaternion.SetLookRotation(transform.position+new Vector3(0,0,-1));
        UnityEngine.Object.Instantiate(explosion, transform.position, quaternion);
        new Vector3(0, 0, -1);
    }
}